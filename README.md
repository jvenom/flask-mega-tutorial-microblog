# Flask Mega-Tutorial microblog sample app

My sample code for the microblog application from Miguel Grinberg's [Flask Mega-Tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world). I do not claim any rights to this code. All code from Miguel's [repository](https://github.com/miguelgrinberg/microblog) is copyrighted by Miguel under the [MIT license](https://github.com/miguelgrinberg/microblog/blob/master/LICENSE).

Here are the changes I've made that differ from Miguel's repository. They are mostly immaterial and irrelevant to the app's logic.
- Use [poetry](https://python-poetry.org/) for dependency management.
- Enable [pre-commit](https://pre-commit.com/) [hooks](.pre-commit-config.yaml).
- Format Python source files with [black](https://black.readthedocs.io/en/stable/).
- Reorder Python imports with [reorder_python_imports](https://github.com/asottile/reorder_python_imports).
- Use `pytest` for the unit tests instead of `unittest`.
